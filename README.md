# JavaScript

A GitLab CI process for managing JavaScript packages.

## Usage

Include the [process](#process) definition into a project's `/.gitlab-ci.yml`
file using

<!-- BEGIN_VERSIONED_TEMPLATE

```yml
include:
  - file: "/.gitlab/ci/process.yml"
    project: "ethima/javascript"
    ref: "v__NEXT_SEMANTIC_RELEASE_VERSION__"
```

-->

```yml
include:
  - file: "/.gitlab/ci/process.yml"
    project: "ethima/javascript"
    ref: "v5.1.2"
```

<!-- END_VERSIONED_TEMPLATE_REPLACEMENT -->

This process is based on the ethima/gitlab-ci> project which provides basic
functionality for managing projects on GitLab, e.g. release and dependency
management, linting of files, etc. Follow the instructions in [the _Usage_
section in its documentation][gitlab-ci-usage-url] to finalize configuration of
the enrollment in this process and/or to learn how to opt-out of jobs enabled
by it.

## [Process](/.gitlab/ci/process.yml)

The [`process`](/.gitlab/ci/process.yml) creates pipelines that include jobs
for generic software management tasks as well as those specific to JavaScript
projects.

[gitlab-ci-usage-url]: https://gitlab.com/ethima/gitlab-ci#usage
