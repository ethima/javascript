# Changelog

## [5.1.2](https://gitlab.com/ethima/javascript/compare/v5.1.1...v5.1.2) (2025-03-03)


### Bug Fixes

* **deps:** update dependency ethima/gitlab-ci to v13.1.3 ([2143e0c](https://gitlab.com/ethima/javascript/commit/2143e0c8f69c3517cf931fa2c189cfa5f9cacde4))

## [5.1.1](https://gitlab.com/ethima/javascript/compare/v5.1.0...v5.1.1) (2025-02-17)


### Bug Fixes

* **deps:** update dependency ethima/gitlab-ci to v13.1.1 ([8f9a0ac](https://gitlab.com/ethima/javascript/commit/8f9a0ac144fd399d66956049c91f99234e2385c8))

# [5.1.0](https://gitlab.com/ethima/javascript/compare/v5.0.0...v5.1.0) (2025-02-11)


### Features

* **deps:** update dependency ethima/gitlab-ci to v13.1.0 ([f42477d](https://gitlab.com/ethima/javascript/commit/f42477d23c4f6f2412284da1f9bd6de8763fa918))

# [5.0.0](https://gitlab.com/ethima/javascript/compare/v4.0.0...v5.0.0) (2024-11-21)


* build(deps)!: update dependency ethima/gitlab-ci to v13 ([3310e5d](https://gitlab.com/ethima/javascript/commit/3310e5dea47cdea64ec2dc8b35ac01e6510b3daf))


### BREAKING CHANGES

* Update to Renovate v39 for dependency management. Clear
any caches in case of user filesystem conflicts. For full details on
potentially necessary configuration changes see
https://github.com/renovatebot/renovate/releases/tag/39.0.0.

# [4.0.0](https://gitlab.com/ethima/javascript/compare/v3.0.0...v4.0.0) (2024-11-04)


* build(deps)!: update dependency ethima/gitlab-ci to v12 ([390c467](https://gitlab.com/ethima/javascript/commit/390c467b89ff4ee9b36a3d57abf5223121cf6c13))


### BREAKING CHANGES

* The version of the `semantic-release` tooling this
release transitively ships with (v24.2.0), leverages the
`commit-analyzer` and `release-notes-generator`` plugins which both
expect to be used with the latest major versions of
`conventional-changelog` packages. If you are installing any of these
packages in addition to `semantic-release`, be sure to update them as
well.

# [3.0.0](https://gitlab.com/ethima/javascript/compare/v2.0.0...v3.0.0) (2024-02-20)


### Build System

* **deps:** update dependency ethima/gitlab-ci to v10 ([a781046](https://gitlab.com/ethima/javascript/commit/a7810462d01a7b710ca48ce90960cf14538c2320))


### BREAKING CHANGES

* **deps:** The `ethima/semantic-release@8` process, which is
incorporated into this process through the ethima/gitlab-ci> project,
depends on `@ethima/semantic-release-configuration@8` and
`semantic-release@23` which in turn depend on `cosmiconfig@9`.
Primarily, this means that if a project is using a `config.js` file, or
an equivalent such as `config.json` or `config.ts`, to configure
`cosmiconfig` itself, the file needs to be moved into a `.config`
directory in the root of the project.

See `cosmiconfig`'s ["Usage for end users"
documentation][cosmiconfig-meta-config-url] for details and its [v9.0.0
release notes][cosmiconfig-v9-release-notes-url].

[cosmiconfig-meta-config-url]: https://github.com/cosmiconfig/cosmiconfig/blob/a5a842547c13392ebb89a485b9e56d9f37e3cbd3/README.md#usage-for-end-users
[cosmiconfig-v9-release-notes-url]: https://github.com/cosmiconfig/cosmiconfig/releases/tag/v9.0.0

# [2.0.0](https://gitlab.com/ethima/javascript/compare/v1.0.0...v2.0.0) (2024-01-05)


* feat(deps)!: update dependency ethima/gitlab-ci to v9 ([9681bed](https://gitlab.com/ethima/javascript/commit/9681bed61ea12fe5b8a28d6c6ae1a9049435e81c))


### BREAKING CHANGES

* The update to ethima/gitlab-ci@9 includes an update to
[@ethima/semantic-release@7](https://gitlab.com/ethima/semantic-release/-/releases/v7.0.0),
which in turn, includes a change to the commit type used for release
commits in the underlying configuration that is used. This type has been
changed from `chore` to `build`. This is only a breaking change for
those with additional workflows on top of the semantic release workflow
provided by this project relying on the structure of these commit
messages.

# 1.0.0 (2023-11-04)


### Features

* **process:** include the ethima/gitlab-ci> process ([7b413cf](https://gitlab.com/ethima/javascript/commit/7b413cf8b1308fe34727990e42d00195056a341c))
